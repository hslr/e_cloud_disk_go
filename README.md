# eCloudDisk

#### 介绍
eCloudDisk一款基于Go语言开发的云盘系统。支持算法去重，离线下载，文件分享，WebDAV协议。由于各种原因暂不开源只提供下载。提供Api接口文档可以自由开发或搭配前端页面。

#### 软件架构
|语言|框架/软件|说明
|---|---|---|
|后端语言/框架|Go语言/Gin web框架|提供API接口文档|
|前端语言/框架|基于layui开发自有框架|根据API可自己开发|
|数据库|Mysql/sqlite|二选一

#### 软件支持系统
理论上支持所有。目前提供 Whidows，Linux版本，树莓派版本下载，其他版本需求可联系作者看是否可以编译一下。


#### 软件应用场景

可搭建私有云，公司，废旧电脑，智能硬件如：树莓派等


#### 安装/下载教程

1. 右侧可以进行发行版的下载
2. 下载运行即可
3. 或者去官网下载[http://ecdisk.enianteam.com/](http://ecdisk.enianteam.com/)


#### 自定义UI（主题模板）安装教程
[自定义主题教程](https://gitee.com/hslr/e_cloud_disk_go/blob/master/%E8%87%AA%E5%AE%9A%E4%B9%89%E6%A8%A1%E6%9D%BF.md)


#### UI前端自由开发说明

> 文档会一直持续更新，会根据后端增加新功能不断进行更新


##### API接口文档（基于ApiPOST编写和测试）

[eCloudDisk 云盘API接口文档](https://docs.apipost.cn/preview/58a12e71e0744fc2/b08b4d06dba09907)

#### 联系作者

QQ:95302870
